/*
 * svcexec - execute command/script as win32 service. See README.
 *
 * Copyright © 2013-2017 Andrey Rys.
 * All rights reserved.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <windows.h>
#include <winsvc.h>

#define svname "svcexec"

static char *cmd;
static DWORD time = 60;
static SERVICE_STATUS svstat;
static SERVICE_STATUS_HANDLE svhandle;
static DWORD pPid;
static HANDLE pHandle;

static BOOL CALLBACK termprocfn(HWND wnd, LPARAM lParam)
{
	DWORD Id;

	GetWindowThreadProcessId(wnd, &Id);
	if (Id == (DWORD)lParam) PostMessage(wnd, WM_CLOSE, 0, 0);
	return TRUE;
}

static void termproc(HANDLE hProc, DWORD Pid)
{
	EnumWindows((WNDENUMPROC)termprocfn, (LPARAM)Pid);
	if (WaitForSingleObject(hProc, 5000) != WAIT_OBJECT_0) TerminateProcess(hProc, 0);
	CloseHandle(hProc);
}

static void svstop(void)
{
	svstat.dwCurrentState = SERVICE_STOP_PENDING;
	SetServiceStatus(svhandle, &svstat);
	termproc(pHandle, pPid);
	svstat.dwControlsAccepted &= ~(SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN);
	svstat.dwCurrentState = SERVICE_STOPPED;
	SetServiceStatus(svhandle, &svstat);
}

static void WINAPI svhandler(DWORD code);

static VOID WINAPI svmain(DWORD argc, LPTSTR *argv)
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	svstat.dwServiceType = SERVICE_WIN32;
	svstat.dwCurrentState = SERVICE_STOPPED;
	svstat.dwControlsAccepted = 0;
	svstat.dwWin32ExitCode = NO_ERROR;
	svstat.dwServiceSpecificExitCode = NO_ERROR;
	svstat.dwCheckPoint = 0;
	svstat.dwWaitHint = 0;

	svhandle = RegisterServiceCtrlHandler(svname, svhandler);
	if (svhandle) {
		svstat.dwCurrentState = SERVICE_START_PENDING;
		SetServiceStatus(svhandle, &svstat);
		svstat.dwControlsAccepted |= (SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN);
		svstat.dwCurrentState = SERVICE_RUNNING;
		SetServiceStatus(svhandle, &svstat);
		if (!cmd) {
			MessageBoxA(0, "Specify command with -c argument passed into binPath of service", NULL, MB_ICONINFORMATION | MB_OK);
			goto _stop;
		}

		do {
			memset(&si, 0, sizeof(si));
			memset(&pi, 0, sizeof(pi));
			si.wShowWindow = SW_SHOW;
			si.cb = sizeof(si);
			CreateProcessA(NULL, cmd, NULL, NULL, FALSE, CREATE_NEW_PROCESS_GROUP, NULL, NULL, &si, &pi);
			pPid = pi.dwProcessId; pHandle = pi.hProcess;
			WaitForSingleObject(pi.hProcess, INFINITE);
			if (time == 0) break;
			Sleep(time * 1000);
		} while (1);

_stop:
		svstop();
	}
}

static void WINAPI svhandler(DWORD code)
{
	switch (code) {
		case SERVICE_CONTROL_INTERROGATE:
			break;

		case SERVICE_CONTROL_SHUTDOWN:
		case SERVICE_CONTROL_STOP:
			svstop();
			return;

		case SERVICE_CONTROL_PAUSE:
			break;

		case SERVICE_CONTROL_CONTINUE:
			break;

		default:
			break;
	}

	SetServiceStatus(svhandle, &svstat);
} 

int main(int argc, char **argv)
{
	SERVICE_TABLE_ENTRY svtab[] = {
		{ svname, svmain },
		{ 0, 0 }
	};
	int i;

	for (i = 0; i < argc; i++) {
		if (strcmp(*(argv+i), "-c") == 0) cmd = *(argv+i+1);
		if (strcmp(*(argv+i), "-t") == 0) time = atoi(*(argv+i+1));
	}

	if (StartServiceCtrlDispatcher(svtab) == FALSE) return GetLastError();

	return 0;
}
